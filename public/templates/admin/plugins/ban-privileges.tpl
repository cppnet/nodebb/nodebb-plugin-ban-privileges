<div class="row">
    <div class="col">
        <div class="panel panel-default">
            <div class="panel-heading">User ban privileges</div>
            <div class="panel-body">
                <form role="form" class="nodebb-plugin-ban-privileges-settings">
                    <div class="form-group">
                        <label for="groupNameCanBanUsers">Allow members of a group to ban users</label>
                        <select class="form-control" id="groupNameCanBanUsers" name="groupNameCanBanUsers">
                            <option value="">None</option>
                            <!-- BEGIN groupNames -->
                            <option value="{groupNames}">{groupNames}</option>
                            <!-- END groupNames -->
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<button id="save" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
    <i class="material-icons">save</i>
</button>