'use strict';

define('admin/plugins/ban-privileges', ['settings'], function (settings) {
    let banPrivileges = {};

    banPrivileges.init = function() {
        settings.load('nodebb-plugin-ban-privileges', $('.nodebb-plugin-ban-privileges-settings'));

        $('#save').on('click', function() {
            settings.save('nodebb-plugin-ban-privileges', $('.nodebb-plugin-ban-privileges-settings'));
        });
    };

    return banPrivileges;
});