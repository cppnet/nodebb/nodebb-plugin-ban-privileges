"use strict";

let plugin = {};
let groups = module.parent.require('./groups');
let meta = module.parent.require('./meta');
let privileges = module.parent.require('./privileges');
let db = module.parent.require('./database');
let winston = module.parent.require('winston');

let async = require('async');


plugin.init = function(params, callback) {
    params.router.get('/admin/ban-privileges', params.middleware.admin.buildHeader, renderAdmin);
    params.router.get('/api/admin/ban-privileges', renderAdmin);

    callback();
};

plugin.addAdminNavigation = function(header, callback) {
    header.plugins.push({
        route: '/ban-privileges',
        icon: 'fa-gavel',
        name: 'Ban Privileges'
    });

    callback(null, header);
};

plugin.canBanUser = function(data, callback) {
    if(data.canBan) {
        callback(null, data);
        return;
    }

    async.parallel({
        banPrivilege: async.apply(hasBanPrivilege, {uid: data.callerUid, canBan: data.canBan}),
        isTargetGlobalMod: async.apply(privileges.users.isGlobalModerator, data.uid)
    }, function (err, result) {
        if(!err && result.banPrivilege.canBan) {
            data.canBan = !data.isTargetAdmin && !result.isTargetGlobalMod;
        }

        callback(err, data);
    });
};

plugin.hasBanPrivilege = function(data, callback) {
    hasBanPrivilege(data, callback);
};


function hasBanPrivilege(data, callback) {
    if(data.canBan) {
        callback(null, data);
        return;
    }

    meta.settings.get('nodebb-plugin-ban-privileges', function(err, settings) {
        if(err) {
            callback(err, data);
        } else {
            groups.isMember(data.uid, settings.groupNameCanBanUsers || '', function (err, isMember) {
                if (!err) {
                    data.canBan = isMember;
                }

                callback(err, data);
            });
        }
    });
}

function getGroupNames(callback) {
    async.waterfall([
        function (next) {
            db.getSortedSetRange('groups:createtime', 0, -1, next);
        },
        function (groupNames, next) {
            groupNames = groupNames.filter(function (name) {
                return name !== 'registered-users' && name !== 'guests' && !groups.isPrivilegeGroup(name);
            });
            groupNames.sort();
            next(null, groupNames);
        },
    ], callback);
}

function renderAdmin(req, res) {
    getGroupNames(function(err, groupNames) {
        if(err) {
            winston.error('[plugin/ban-privileges] Unable to retrieve group names: ' + err.message);
        }

        res.render('admin/plugins/ban-privileges', {
            groupNames: groupNames || []
        });
    });
}

module.exports = plugin;
