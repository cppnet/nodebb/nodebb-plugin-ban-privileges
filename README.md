# nodebb-plugin-ban-privileges

Give the privilege to ban accounts to a group in your NodeBB. This plugin was inspired by `nodebb-plugin-superusers`. The plugin should work with NodeBB 1.5+.

After installing and activating the plugin, go to the plugins settings page in the ACP. There you can choose the group that should receive privileges to ban other people. The members of the group won't be able to ban administrators or global moderators.

Repository: https://gitlab.com/cppnet/nodebb/nodebb-plugin-ban-privileges